const BASE_CART_URL = "https://647f22a0c246f166da902480.mockapi.io/cart";

axios({
    url: BASE_CART_URL,
    method: "GET"
}).then(function(res){
    renderCart(res.data);
}).catch(function(err){
    console.log(err);
}
)

var cartServices = {
    getListCart: () => {
        return axios({
            url: BASE_CART_URL,
            method: "GET"
            }).then(function(res){
                return res.data;
            }
        );
    },
    addToCart: (data) => {
        return axios({
            url: BASE_CART_URL,
            method: "POST",
            data: data
            })
    },
    updateCart: (id, data) => {
        return axios({
            url: `${BASE_CART_URL}/${id}`,
            method: "PUT",
            data: data
            })
    },
    deleteCartById: (id) => {
        return axios({
            url: `${BASE_CART_URL}/${id}`,
            method: "DELETE"
            })
    }
}