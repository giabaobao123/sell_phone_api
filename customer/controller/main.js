var cartArr = [];
axios({
  url: BASE_CART_URL,
  method: "GET",
})
  .then(function (res) {
    cartArr = res.data;
    var totalQuantity = 0;
    cartArr.forEach(function (cart) {
      totalQuantity += cart.quantity;
    });
    document.querySelector("#cartCount").innerHTML = totalQuantity;
  })
  .catch(function (err) {
    console.log(err);
  });
function fetchProduct() {
  productServices
    .getListProduct()
    .then(function (res) {
      renderProduct(res);
    })
    .catch(function (err) {
      console.log(err);
    });
}

function fetchCartQuantity() {
  var totalQuantity = 0;
  cartArr.forEach(function (cart) {
    totalQuantity += cart.quantity;
  });
  document.querySelector("#cartCount").innerHTML = totalQuantity;
}
fetchCartQuantity();
function getPhoneByType() {
  document.querySelector("#productList").innerHTML = "";
  const type = document.querySelector("#productBrand").value;
  if (type === "all") {
    productServices.getListProduct().then(function (res) {
      renderProduct(res);
    });
    return;
  }
  productServices.getListProduct().then(function (res) {
    var filterPhone = res.filter(function (phone) {
      return phone.type === type;
    });
    renderProduct(filterPhone);
  });
}

function findProductInCart(id) {
  var cartItem = cartArr.find(function (item) {
    return item.product.id == id;
  });
  return cartItem;
}

function addProductToCart(id) {
  //check if product exist in cart add quantity by 1
  //else add new product to cart
  var cartItem = findProductInCart(id);
  if (cartItem) {
    var quantity = cartItem.quantity * 1;
    cartItem.quantity = quantity + 1;
    cartServices.updateCart(cartItem.id, cartItem).then(function (res) {
      var index = cartArr.findIndex(function (item) {
        return item.id == cartItem.id;
      });
      cartArr[index] = res.data;
      Swal.fire({
        title: "Add to cart successfully",
        text: "Product exist in cart, update quantity by 1",
        icon: "success",
        confirmButtonText: "Continue shopping",
      });
      renderCart(cartArr);
      fetchCartQuantity();
    });
  } else {
    productServices.getProductById(id).then(function (res) {
      var product = res.data;
      var quantity = 1;
      data = {
        product: product,
        quantity: quantity,
      };
      cartServices.addToCart(data).then(function (res) {
        cartArr.push(res.data);
        Swal.fire({
          title: "Add to cart successfully",
          icon: "success",
          confirmButtonText: "Continue shopping",
        });
        renderCart(cartArr);
        fetchCartQuantity();
      });
    });
  }
}

function updateCartQuantity(id, quantity) {
  cartServices.getListCart().then(function (res) {
    var product = res.find(function (item) {
      return item.id == id;
    });
    product.quantity = quantity;
    cartServices.updateCart(id, product).then(function (res) {});
  });
  fetchCartQuantity();
}

function increaseQuantity(id) {
  var product = "#productQuantity" + id;
  var quantity = document.querySelector(product).value * 1;
  var index = cartArr.findIndex(function (item) {
    return item.id == id;
  });
  cartArr[index].quantity = quantity + 1;
  renderCart(cartArr);
  updateCartQuantity(id, quantity + 1);
}

function decreaseQuantity(id) {
  var product = "#productQuantity" + id;
  var quantity = document.querySelector(product).value * 1;
  if (quantity > 1) {
    var index = cartArr.findIndex(function (item) {
      return item.id == id;
    });
    cartArr[index].quantity = quantity - 1;
    renderCart(cartArr);
    updateCartQuantity(id, quantity - 1);
  }
}

function deleteCartItem(id) {
  cartServices.deleteCartById(id).then(function (res) {
    var index = cartArr.findIndex(function (item) {
      return item.id == id;
    });
    cartArr.splice(index, 1);
    renderCart(cartArr);
    fetchCartQuantity();
  });
}

function deleteAllCart() {
  Swal.fire({
    title: "Delete cart?",
    text: "You won't be able to revert this!",
    icon: "warning",
    showCancelButton: true,
    confirmButtonColor: "#3085d6",
    cancelButtonColor: "#d33",
    confirmButtonText: "Yes, delete it!",
    reverseButtons: true,
  }).then((result) => {
    if (result.isConfirmed) {
      Swal.fire(
        "Deleted!",
        "All item in your cart has been deleted!.",
        "success"
      );
      cartArr.forEach(function (cart) {
        cartServices.deleteCartById(cart.id);
      });
      cartArr = [];
      renderCart(cartArr);
      fetchCartQuantity();
    }
  });
}

function checkOut() {
  Swal.fire({
    title: "Check out?",
    text: "You won't be able to revert this!",
    icon: "warning",
    showCancelButton: true,
    confirmButtonColor: "#3085d6",
    cancelButtonColor: "#d33",
    confirmButtonText: "Check out",
    reverseButtons: true,
  }).then((result) => {
    if (result.isConfirmed) {
      Swal.fire("Check out!", "Thank you for your purchase!.", "success");
      cartArr.forEach(function (cart) {
        cartServices.deleteCartById(cart.id);
      });
      cartArr = [];
      renderCart(cartArr);
      fetchCartQuantity();
    }
  });
}
