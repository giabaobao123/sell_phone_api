let renderListSP = (dssp) => {
  let contentHTML = "";
  dssp.forEach((item) => {
    let content = `<tr>
        <td>${item.id}</td>
        <td>${item.name}</td>
        <td>${(Number(item.price).toLocaleString())}Vnđ</td>
        <td class="text-center"><img src="${item.img}" class="img" alt="" /></td>
        <td>${item.desc}</td>
        <td><button class="btn btn-danger" onclick = "xoaSP(${item.id})">Delete<i class="fa fa-trash pl-3"></i></button></td>
        <td><button class="btn btn-success" onclick = "suaSP(${item.id})"  data-bs-toggle="modal" data-bs-target="#exampleModal">Edit <i class="fa fa-edit"></i></button></td>
        </tr>`;
    contentHTML += content;
  });
  document.getElementById("tBodySP").innerHTML = contentHTML;
  document.getElementById("tBodySP").style.fontWeight = "bold";
};
// lấy thông tin từ sản phẩm trên form
let layThongTinTuForm = () => {
  let name = document.getElementById("phoneName").value;
  let price = document.getElementById("phonePrice").value;
  let screen = document.getElementById("phoneScreen").value;
  let backCamera = document.getElementById("backCamera").value;
  let frontCamera = document.getElementById("frontCamera").value;
  let img = document.getElementById("phoneImg").value;
  let desc = document.getElementById("desc").value;
  let type = document.getElementById("type").value;
  console.log(type.innerHTML)

  return {
    name,
    price,
    screen,
    backCamera,
    frontCamera,
    img,
    desc,
    type,
  };
};
layThongTinTuForm()
// 
let showThongTinLenForm = (sp) => {
  let{name,price,screen,backCamera,frontCamera,img,desc,type} = sp
  
   document.getElementById("phoneName").value = name 
   document.getElementById("phonePrice").value = price
   document.getElementById("phoneScreen").value = screen
   document.getElementById("backCamera").value = backCamera
   document.getElementById("frontCamera").value = frontCamera
   document.getElementById("phoneImg").value = img
   document.getElementById("desc").value = desc
   document.getElementById("type").value = type
}

// lọc sản phẩm theo giá
function SortList (dssp) {
  for (let i=0;i<dssp.length;i++) {
    for (let j=i+1;j<dssp.length;j++) {
        if (Number(dssp[i].price)<Number(dssp[j].price)) {
            tempt = dssp[i]
            dssp[i] = dssp[j]
            dssp[j] = tempt 
        }
    }
}
}