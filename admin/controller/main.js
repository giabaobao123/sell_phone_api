let idUpdate = null;
// click vào add phone sẽ có nút addphone
let addPhoneButton = () => {
  document.getElementById("upDate").style.display = "none";
  document.getElementById("addPhone").style.display = "inline-block";
  document.getElementById("formPhone").reset()
};
// get và render dssp
function fetchListPhone() {
  productServ
    .getList()
    .then((res) => {
      renderListSP(res.data);
    })
    .catch((err) => {});
}
fetchListPhone();
// post sp
let addPhone = () => {
  let phone = layThongTinTuForm();
  if (isValidator()) {
    productServ
      .create(phone)
      .then((res) => {
        contentFilter()
        $("#exampleModal").modal("hide");
        swal({
          text: "Add phone sucessfully!",
          icon: "success",
        });
        document.getElementById("formPhone").reset();
      })
      .catch((err) => {
        console.log(err);
      });
  }
};
// xóa sư
let xoaSP = (id) => {
  // khi click hiện alert thông báo có chắc muốn xóa không
  swal({
    title: "Are you sure?",
    text: "This phone will be deleted, you can't undo this action",
    icon: "warning",
    buttons: true,
    dangerMode: true,
    // khi xóa nếu xóa và render thành công sẽ hiện ra alert đã xóa thành công
  }).then((willDelete) => {
    if (willDelete) {
      productServ
        .delete(id)
        .then((res) => {
          contentFilter()
          swal("Poof! Your imaginary file has been deleted!", {
            icon: "success",
          });
        })
        .catch((err) => {});
    }
  });
};
// sửa và cập nhật
let suaSP = (id) => {
  idUpdate = id;
  document.getElementById("addPhone").style.display = "none";
  document.getElementById("upDate").style.display = "inline-block";
  productServ
    .getID(id)
    .then((res) => {
      showThongTinLenForm(res.data);
    })
    .catch((err) => {
      console.log(err);
    });
};

let capNhat = () => {
  let phone = layThongTinTuForm();
  if (isValidator()) {
    productServ
      .update(idUpdate, phone)
      .then((res) => {
        contentFilter()
        $("#exampleModal").modal("hide");
        swal({
          text: "Update phone sucessfully!",
          icon: "success",
        });
      })
      .catch((err) => {
        console.log(err);
      });
  }
};

let findNameInList = () => {
  let search = document.getElementById("search");
  let newArr = [];
  let input = search.value;
  productServ
    .getList()
    .then((res) => {
      let dssp = res.data;
      if (input.length > 0) {
        dssp.forEach(function (item) {
          let name = item.name;
          if (name.toUpperCase().includes(input.toUpperCase())) {
            newArr.push(item);
            renderListSP(newArr);
          } else {
            renderListSP(newArr);
          }
        });
      } 
      else {
        contentFilter()
      }
    })
    .catch((err) => {
      console.log(err);
    });
};
// khi fiter onchange sẽ thực thi content
let filterPrice = () => {
  contentFilter()
};
// nội dung của các options
let contentFilter = function() {
  let value = document.getElementById("filter").value;
  if (value == "1") {
    productServ
      .getList()
      .then(function (res) {
        let dssp = res.data;
        SortList(dssp);
        renderListSP(dssp);
      })
      .catch(function (err) {
        console.log(err);
      });
  } else if (value == "2") {
    productServ
      .getList()
      .then(function (res) {
        let dssp = res.data;
        SortList(dssp);
        renderListSP(dssp.reverse());
      })
      .catch(function (err) {
        console.log(err);
      });
  } else {
    fetchListPhone();
  }
}